import java.util.UUID;

public class Tea extends Wood {
    String type;

    Tea (String type, UUID id){
        this.type = type;
        super.id = id;
        super.create();
    }

    Tea (){}

    public String getType() {

        return type;
    }
//
//    public int getCount() {
//        return super.count;
//    }
//
//    public UUID getID() {
//        return super.id;
//    }
//
//    public String getName() {
//        return super.name;
//    }
//
//    public double getPrice() {
//        return super.price;
//    }
//
//    public String getFirma() {
//        return super.firma;
//    }
//
//    public String getCountry() {
//        return super.country;
//    }

    public void create() {
        super.name = "random Name";
        super.id = UUID.randomUUID();
        super.price = Math.random();
        super.firma = "firma " + Math.random();
        super.country = "country " + Math.random();
    }

    @Override
    public void read() {

        System.out.println("typeOfTea = " + type);
        super.read();

    }

    @Override
    public void update(String type, UUID id,String name, double price, String firma, String country) {
        this.type = type;
        super.id = id;
        super.name = name;
        super.price = price;
        super.firma = firma;
        super.country = country;
    }

    @Override
    public void delete() {
        type = null;
        super.delete();

    }
}
