import java.util.Scanner;
import java.util.UUID;

public class Main {
    public static Boolean flag = false;

    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        Tea[] arrTea = null;
        Coffe[] arrCoffe = null;

        if (args[0].equals("Tea")) {
            arrTea = new Tea[Integer.parseInt(args[1])];
            flag = false;
            for (int i = 0; i < Integer.parseInt(args[1]); i++) {
                arrTea[i] = new Tea("typeOfTea" + i, UUID.randomUUID());
                System.out.println("typeTea, ID, name, price, firma, countru");
                arrTea[i].update(in.next(), UUID.fromString(in.next()), in.next(), in.nextDouble(), in.next(), in.next());
                System.out.println("____________________________");
                arrTea[i].read();
                System.out.println("____________________________");
            }
//t1 1ce0e821-7677-4176-9e8a-54f63d0b00ea n1 10 f1 c1 

        } else if (args[0].equals("Coffe")) {
            arrCoffe = new Coffe[Integer.parseInt(args[1])];
            flag = true;
            for (int i = 0; i < Integer.parseInt(args[1]); i++) {
                arrCoffe[i] = new Coffe("typeOfCoffe" + i, UUID.randomUUID());
                System.out.println("typeCoffe, ID, name, price, firma, countru");
                arrCoffe[i].update(in.next(), UUID.fromString(in.next()), in.next(), in.nextDouble(), in.next(), in.next());
                System.out.println("____________________________");
                arrCoffe[i].read();
                System.out.println("____________________________");
            }
        } else {
            System.out.println("err input");
            System.exit(2);
        }
        System.out.println("EXIT? (EXIT/NO)");
        while (in.next().equals("EXIT") == false) {
            if ((in.next().equals("update")) & (flag = true)) {
                System.out.println("number of redaction object?");
                int r = in.nextInt() + 1;
                System.out.println("typeTea, ID, name, price, firma, countru");
                arrTea[r].update(in.next(), UUID.fromString(in.next()), in.next(), in.nextDouble(), in.next(), in.next());
                System.out.println("__________________________");
                arrTea[r].read();
                System.out.println("__________________________");
            }

            if ((in.next().equals("update")) & (flag = false)) {
                System.out.println("number of redaction object?");
                int r = in.nextInt() + 1;
                System.out.println("typeCoffe, ID, name, price, firma, countru");
                arrCoffe[r].update(in.next(), UUID.fromString(in.next()), in.next(), in.nextDouble(), in.next(), in.next());
                System.out.println("__________________________");
                arrCoffe[r].read();
                System.out.println("__________________________");
            }

        }
        System.exit(1);

    }
}


