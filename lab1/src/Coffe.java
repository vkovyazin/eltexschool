import java.util.UUID;

public class Coffe extends Wood{
    String type;

    Coffe (String type, UUID id){
        this.type = type;
        super.id = id;
        super.create();
    }

    @Override
    public void create( ) {
        super.name = "random Name";
        super.id = UUID.randomUUID();
        super.price = Math.random();
        super.firma = "firma " + Math.random();
        super.country = "country " + Math.random();
    }

    @Override
    public void read() {
        System.out.println("typeOfCoffie = " + type);
        super.read();
    }

    @Override
    public void update(String type, UUID id, String name, double price, String firma, String country) {
        this.type = type;
        super.id = id;
        super.name = name;
        super.price = price;
        super.firma = firma;
        super.country = country;

    }

    @Override
    public void delete() {
        type = null;
        super.delete();
    }


}
