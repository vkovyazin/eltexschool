import java.util.UUID;

public interface ICrudAction {
    void create();
    void read();
    void update(String type, UUID id, String name, double price, String firma, String country);
    void delete();
}
