import javax.xml.bind.SchemaOutputResolver;
import java.util.UUID;

public abstract class Wood implements ICrudAction {
    private static int count;
    protected UUID id;
    protected String name;
    protected double price;
    protected String firma;
    protected String country;

//    Wood(){
//        id = UUID.randomUUID();
//    }
//
//    Wood(UUID uuid){
//
//        this.id = uuid;
//    }

    @Override
    public void create() {

        count++;
    }

    @Override
    public void read() {
        System.out.println("name = " + name);
        System.out.println("count = " + count);
        System.out.println("country = " + country);
        System.out.println("firma = " + firma);
        System.out.println("ID = " + id);
        System.out.println("price = " + price);
    }

//    @Override
//    public void update(String type, UUID id, String name, double price, String firma, String country) {
//    }

    @Override
    public void delete() {
        count--;
        name = null;
        id = null;
        price = 0;
        firma =null;
        country = null;

    }
}
