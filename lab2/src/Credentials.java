public class Credentials {

    private int id;
    private String fio;
    private String email;

    public Credentials(int id, String fio, String email) {
        this.id = id;
        this.fio = fio;
        this.email = email;
    }

    public void getAll() {
        System.out.println("id = " + id);
        System.out.println("fio = " + fio);
        System.out.println("email = " + email);
    }
}
